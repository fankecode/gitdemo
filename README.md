
###  **基于springboot智慧物业管理系统**
 

#### 介绍
智慧物业管理系统是将现代化物业管理理念和计算机网络结合起来的一种物业管理方式，基于网络的物业管理软件已经逐步发展成为现代化的网络物业管理信息信息系统。

#### 软件架构
软件架构说明
springboot+layui
响应式布局
![前台](https://images.gitee.com/uploads/images/2022/0509/221430_2abf589c_7619101.png "Screenshot 2022-05-09 221307.png")

![前台](Screenshot%202022-05-09%20221347.png)
![前台](Screenshot%202022-05-09%20221403.png)
![后台](Screenshot%202022-04-22%20214854.png)

#### 安装教程

1.  jdk;idea
2.  maven;
3.  mysql;

#### 使用说明

1.  数据库mysql5.3.7
2.  maven3.3.9
3.  jdk1.8
4.需要联系QQ：2053762897

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)